﻿using DB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace codeFirstApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeerController : ControllerBase
    {
        private readonly BarContext _barContext;

        public BeerController(BarContext context)
        {
            this._barContext = context;
        }

        [HttpGet]
        public IEnumerable<Beer> Get() => _barContext.Beers.ToList();
    }
}
